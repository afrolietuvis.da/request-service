package com.requestservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private BigDecimal amount;

    private String currency;

    private String paymentType;

    private String debtorIban;

    private String creditorIban;

    private String creditorBIC;

    private String details;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyy-MM-dd HH:mm")
    private LocalDateTime createdOn;

   public static Request of(RequestDto requestDto){
        Request request = new Request();
        request.setAmount(requestDto.getAmount());
        request.setCreditorBIC(requestDto.getCreditorBIC());
        request.setCurrency(requestDto.getCurrency());
        request.setPaymentType(requestDto.getPaymentType());
        request.setCreditorIban(requestDto.getCreditorIban());
        request.setDebtorIban(requestDto.getDebtorIban());
        request.setDetails(requestDto.getDetails());
        request.setCreatedOn(LocalDateTime.now());
        return request;
   }
}
