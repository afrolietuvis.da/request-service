package com.requestservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@NoArgsConstructor
public class PaymentDto {

        private BigDecimal amount;

        private String currency;

        private String paymentType;

        private String debtorIban;

        private String creditorIban;

        private String creditorBIC;

        private String details;
}
