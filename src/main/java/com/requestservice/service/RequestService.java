package com.requestservice.service;

import com.requestservice.error.RestErrorHandler;
import com.requestservice.model.Request;
import com.requestservice.model.RequestDto;
import com.requestservice.repository.RequestRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Service
public class RequestService {

    private final RequestRepository repository;
    private final RestTemplate restTemplate;

    public RequestService(RequestRepository repository, RestTemplateBuilder builder) {
        this.repository = repository;
        this.restTemplate = builder.
                setConnectTimeout(Duration.ofMillis(3000)).
                setReadTimeout(Duration.ofMillis(3000)).
                build();
        restTemplate.setErrorHandler(new RestErrorHandler());
    }

    public void createRequest(RequestDto requestDto){
       this.repository.save(Request.of(requestDto));
       this.delegateToPaymentService(requestDto);
    }
    public void delegateToPaymentService(RequestDto requestDto){
        restTemplate.postForObject("http://localhost:8082/payments",requestDto, RequestDto.class);
    }
}
