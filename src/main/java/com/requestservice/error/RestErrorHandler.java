package com.requestservice.error;

import lombok.SneakyThrows;
import lombok.extern.java.Log;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Log
@ControllerAdvice
public class RestErrorHandler extends DefaultResponseErrorHandler {


    @SneakyThrows
    @Override public void handleError(ClientHttpResponse response) throws IOException {
        log.warning(StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        throw new Exception("Something happened");
    }

}
