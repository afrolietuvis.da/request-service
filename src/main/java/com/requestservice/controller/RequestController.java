package com.requestservice.controller;

import com.requestservice.model.Request;
import com.requestservice.model.RequestDto;
import com.requestservice.service.RequestService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/requests")
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @PostMapping
    void createRequest(@RequestBody RequestDto request){
     requestService.createRequest(request);
}

}
